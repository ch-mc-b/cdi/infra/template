# Zugriffs Informationen

variable "url" {
  description = "Evtl. URL fuer den Zugriff auf das API des Racks Servers"
  type        = string
  default     = "http://10.6.37.8:5240/MAAS"  
}

variable "key" {
  description = "API Key, Token etc. fuer Zugriff"
  type        = string
  sensitive   = true
  default     = "..."  
}

variable "vpn" {
  description = "Optional VPN welches eingerichtet werden soll"
  type        = string
  default     = "10-6-37-0"  
}

variable "host_no" {
    description = "Host-No fuer die erste Host-IP Nummer"
    type    = number
    default = 30
}
